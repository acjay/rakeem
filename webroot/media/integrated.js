soundManager.setup({
    flashVersion: 9,
    preferFlash: false, // for visualization effects
    useHighPerformance: true, // keep flash on screen, boost performance
    wmode: 'transparent', // transparent SWF, if possible
    url: '/media/soundmanager2/swf'
});

// custom page player configuration
var PP_CONFIG = {
    autoStart: false, // begin playing first sound when page loads
    playNext: true, // stop after one sound, or play through list until end
    useThrottling: false, // try to rate-limit potentially-expensive calls (eg. dragging position around)</span>
    usePeakData: true, // [Flash 9 only] whether or not to show peak data (left/right channel values) - nor noticable on CPU
    useWaveformData: false, // [Flash 9 only] show raw waveform data - WARNING: LIKELY VERY CPU-HEAVY
    useEQData: false, // [Flash 9 only] show EQ (frequency spectrum) data
    useFavIcon: false,     // try to apply peakData to address bar (Firefox + Opera) - performance note: appears to make Firefox 3 do some temporary, heavy disk access/swapping/garbage collection at first(?) - may be too heavy on CPU
    updatePageTitle: false
};

/*******************************************************************************
 *  Configuration variables
 */

var visualizationDepth = 2;
var histogramWordCount = 5;

var wordcloudWidth = 250,
    wordcloudHeight = 150,
    wordcloudMinFontSize = 12,
    wordcloudMaxFontSize = 100;

/*******************************************************************************
 *  Global state - Set variables here if they should be initialized
 *  only once.  Set them in loadPageForData if they should be
 *  reinitialized when visiting new clusters
 */

// Stores the data for all clusters, indexed by the relative URL (i.e.
// Cluster0/Cluster1/.  This allows convenient mapping from URL to
// cluster
var allData = {};

// Flag set to false if full-data load succeeds, otherwise, triggers
var incrementalLoad = true;

// Stores selected node if a node was clicked, otherwise undefined
var selectedNode;

// Used for detecting double-clicks
var lastClick = null;

//  Stores current cluster URL (rather than using window.location.pathname
var currentClusterUrl;

/*******************************************************************************
 *  Utility functions
 */

function isNull(v) {
    return v === null;
}

function isUndefined(v) {
    return typeof v === 'undefined';
}

// Used to create unique element ID's for clusters
function idFromPath(url) {
    return url.replace(/\//g, '');
}

$(document).ready(function () {
    var History = window.History;
    var colorMap;

    // Stores reference to sunburst method object
    var sunburst;

    /***************************************************************************
     *  High level functionality
     *
     *  These methods should be invoked to perform state changes on the page
     *  level.  They operate on the lower-level functionality.
     */

    // (Re)loads the page from a URL (which doubles as a cluster identifier)
    function loadPageForData(url, loaded) {
        if (isUndefined(url) || url === '') {
            url = '/';
        }

        // Reset global state vars
        selectedNode = null;
        currentClusterUrl = url;

        // Time for debugging
        var startTime = new Date();

        if (incrementalLoad) {
            $('#loading-modal').modal('show');
        }
        getRequiredObjects(url, visualizationDepth, function () {}, function (d) {
            console.debug(d.totalRecords + ' records retrieved, ' +
                d.fromServer + ' from server in ' +
                ((new Date() - startTime) / 1000) + ' seconds)');

            // Set color scheme based on current cluster
            setColorScheme(url);

            // Create sunburst diagram
            $('#sunburst').empty();
            sunburst = createSunburst(subclusterVisualizationData(url, visualizationDepth),
                ancestorsVisualizationData(url),
                '#sunburst'
            );

            // Default to current cluster's details
            deselectObject();

            // Update title
            var $title = $('#title');
            $title.empty();
            $title.append(ich.titleTemplate({
                ancestors: allData[url].ancestors,
                clusterIndex: url === '/' ? 'Root' : url.slice(-2, -1)
            }));

            // Show playlist container
            $('#playlistContainer').show();

            if (incrementalLoad) {
                $('#loading-modal').modal('hide');
            }
        });
    }

    // For reseting the color scheme based on the histogram of the
    // currently selected node
    function setColorScheme(url) {
        var color = d3.scale.category20();

        colorMap = allData[url].hist.reduce(function (acc, item, index) {
            acc[item.w] = color(index);
            return acc;
        }, {});

    }

    // Reload the page for a different cluster
    function navigateToCluster(url) {
        History.pushState({url: url}, '', url);
    }

    // Add a track to the playlist
    function enqueueTrack(clusterUrl, index) {
        var clusterData = allData[clusterUrl],
            trackData = clusterData.tracks[index],
            tooltipText =  "Track location: " + clusterUrl + ' Track ' + index + "\n" +
                "Title:  " + trackData.title + "\n" +
                "Artist: " + trackData.artist + "\n" +
                "Album:  " + trackData.album + "\n" +
                "Genre:  " + trackData.genre + "\n",
            playlistItem = ich.playlistItemTemplate({
                audioRoot: clusterData.audioRoot,
                url: trackData.url,
                title: trackData.title,
                artist: trackData.artist,
                tooltipText: tooltipText
            });
        $('#playlist').append(playlistItem);
    }

    // Select a cluster or track
    function selectObject(clusterUrl, trackIndex) {
        hideHoverDetails();
        selectedNode = {
            clusterUrl: clusterUrl,
            trackIndex: trackIndex,
            id: isUndefined(trackIndex) ? clusterUrl : clusterUrl + 'track' + trackIndex
        };
        if (isUndefined(trackIndex)) {
            showClusterDetails(clusterUrl, 0, 'Selected cluster', '#currentDetails', true);
        } else {
            showTrackDetails(clusterUrl, trackIndex, 'Selected track', '#currentDetails', true);
        }
        sunburst.showSelection();
    }

    // Unselect whatever has been selected
    function deselectObject() {
        selectedNode = null;
        showClusterDetails(currentClusterUrl, 0, 'Current cluster', '#currentDetails');
    }

    // Pop-up the track details upon hovering
    function showHoverDetails(clusterUrl, trackIndex, x, y) {
        function postRender() {
            var $hoverDetails = $('#hoverDetails');
            $hoverDetails.css('position', 'absolute');

            //Try to position the popup away from the sunburst

            if (y < window.innerHeight / 2 + window.scrollY) {
                console.debug('Top corner');
                $hoverDetails.css('top', y + 5);
            } else {
                console.debug('Bottom corner');
                $hoverDetails.css('top', y - parseFloat($hoverDetails.css('height')) - 5);
            }

            var $sunburstDiv = $('#sunburst');
            if (x < $sunburstDiv.position().left + $sunburstDiv.width() / 2) {
                $hoverDetails.css('left', Math.max(5, x - parseFloat($hoverDetails.css('width')) - parseFloat($hoverDetails.css('padding-right')) * 2 - 20));
            } else {
                console.debug('Right side');
                $hoverDetails.css('left', x + 20);
            }

            $hoverDetails.fadeIn(100);
        }

        if (isUndefined(trackIndex)) {
            showClusterDetails(clusterUrl, 0, 'Cluster details', '#hoverDetails', undefined, postRender);
        } else {
            showTrackDetails(clusterUrl, trackIndex, 'Track details', '#hoverDetails', undefined, postRender);
        }
    }

    // Hide the ohover popup
    function hideHoverDetails() {
        hideDetails('#hoverDetails');
    }


    /*
     *  Low-level controller logic
     */

    // Creates a wordcloud for a cluster
    function createWordcloud(url, container, onFinished) {
        if (allData[url].hist.length === 0) {
            onFinished();
            return;
        }

        var data = allData[url],
            wordcloudFont = "Impact",
            minWordCount = data.hist.reduce(function (prev, d) { return Math.min(prev, d.c); }, data.hist[0].c),
            totalWordCount = data.hist.reduce(function (prev, d) { return prev + d.c; }, 0),
            fontSize = function (d) {
                return (wordcloudMaxFontSize - wordcloudMinFontSize) * (d.c - minWordCount) / totalWordCount + wordcloudMinFontSize;
            },
            fill = d3.scale.category20(),
            layout = d3.layout.cloud()
                .size([wordcloudWidth, wordcloudHeight])
                .timeInterval(10)
                .words(data.hist)
                .text(function (d) { return d.w; })
                .fontSize(function (d) { return fontSize(d); })
                .font(wordcloudFont)
                .rotate(function (d) { return ~~(Math.random() * 2) * 90; })
                .padding(1)
                .on("end", function (words) {
                    d3.select(container).append("svg")
                        .attr("width", "100%")
                        .attr("height", "100%")
                        .attr("viewBox", "0 0 " + wordcloudWidth + " " + wordcloudHeight)
                        .style("fill", "#ccf")
                        .append("g")
                        .attr("transform", "translate(" + wordcloudWidth / 2 + "," + wordcloudHeight / 2 + ")")
                        .selectAll("text")
                        .data(words)
                        .enter().append("text")
                        .style("font-size", function(d) { return d.size + "px"; })
                        .style("font-family", wordcloudFont)
                        .style("fill", function (d, i) { return fill(i); })
                        .attr("text-anchor", "middle")
                        .attr("transform", function (d) {
                            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                        })
                        .text(function (d) { return d.text; });

                    if (!isUndefined(onFinished)) {
                        onFinished();
                    }
                })
                .start();
    }

    // Creates a details section for a cluster
    function showClusterDetails(url, depth, label, targetSelector, showNav, postRender) {
        var data = allData[url],
            wordcloudElemId = 'wordcloud' + Date.now(), /* url.replace(/\//g, ''), */ // In case the user mouses out quickly
            detailsRendered = ich.clusterDetailsTemplate({
                url: url,
                label: label,
                wordcloudElemId: wordcloudElemId,
                depth: depth,
                size: data.size,
                hist: data.hist.slice(0, histogramWordCount),
                representatives: data.representatives,
                showNav: isUndefined(showNav) ? false : showNav
            }),
            $details = $(targetSelector);


        $details.stop(true, true);
        $details.hide();
        $details.empty();
        $details.append(detailsRendered);
        createWordcloud(url, '#' + wordcloudElemId, postRender);

        // postRender is used to handle positioning and showing the details.
        // If it is undefined, just show the details regardless
        if (isUndefined(postRender)) {
            $details.show();
        }
    }

    // Creates a details section for a track
    function showTrackDetails(url, trackIndex, label, targetSelector, showNav, postRender) {
        var trackData = allData[url].tracks[trackIndex],
            detailsRendered = ich.trackDetailsTemplate({
                label: label,
                artist: trackData.artist,
                title: trackData.title,
                album: trackData.album,
                trackNum: trackData.trackNum,
                genre: trackData.genre,
                showNav: isUndefined(showNav) ? false : showNav
            }),
            $details = $(targetSelector);

        $details.stop(true, true);
        $details.hide();
        $details.empty();
        $details.append(detailsRendered);
        $details.show();
        if (!isUndefined(postRender)) {
            postRender();
        }
    }

    // Hides a details section
    function hideDetails(targetSelector) {
        $(targetSelector).hide();
    }

    // Creates the SVG sunburst visualization
    function createSunburst(root, ancestors, targetSelector) {
        /* Helper functions */

        // Determine base colors for subclusters
        function baseColor(d) {
            var color;
            if (d.isCluster) {
                if (!isUndefined(colorMap[d.topWord])) {
                    if (!isUndefined(d.depth)) {
                        color = d3.interpolateRgb(colorMap[d.topWord], 'rgb(255, 255, 255)')((d.depth - 1) / 3); // Dependent on max depth
                    } else {
                        color = colorMap[d.topWord];
                    }
                } else {
                    color = '#744'
                }
            } else {
                color = '#447';
            }
            return color;
        }

        // Deterimine darkened colors for hover state
        function darkenedColor(d) {
            return d3.rgb(baseColor(d)).darker(2);
        }

        function brightenedColor(d) {
            return d3.rgb(baseColor(d)).brighter(0.5);
        }

        // Stash the old values for transition.
        function stash(d) {
            d.x0 = d.x;
            d.dx0 = d.dx;
        }

        // Interpolate the arcs in data space.
        function arcTween(a) {
            var i = d3.interpolate({x: a.x0, dx: a.dx0}, a);
            return function (t) {
                var b = i(t);
                a.x0 = b.x;
                a.dx0 = b.dx;
                return arc(b);
            };
        }

        // Execute the "double-click" action for a cluster or track
        function actionOnNode(d) {
            if (d.isCluster) {
                navigateToCluster(d.url);
            } else {
                enqueueTrack(d.clusterUrl, d.index);
            }
        }

        // Handle hover-start
        function onMouseover(d) {
            console.debug('Mouseover ' + d.id + ' at page(' + d3.event.pageX + ',' + d3.event.pageY + ') client(' + d3.event.clientX + ',' + d3.event.clientY + ')');
            var xPos = d3.event.pageX, yPos = d3.event.pageY;
            if (isNull(selectedNode) || d.id !== selectedNode.id) {
                d3.select(this).style("fill", darkenedColor(d));
                if (d.isCluster) {
                    showHoverDetails(d.url, undefined, xPos, yPos);
                } else {
                    showHoverDetails(d.clusterUrl, d.index, xPos, yPos);
                }
            }
        }

        // Handle hover-end
        function onMouseout(d) {
            if (isNull(selectedNode) || d.id !== selectedNode.id) {
                d3.select(this).style("fill", baseColor(d));
            }
            hideHoverDetails();
        }

        // Handle clicks and double-clicks
        function onClick(d) {
//                    History.pushState({url: d.url}, '', d.url);

            // Double-click handler
            if (!isNull(lastClick) && lastClick.id === d.id && Date.now() - lastClick.time < 500) {
                actionOnNode(d);
            }

            lastClick = {
                id: d.id,
                time: Date.now()
            };

            if (!isNull(selectedNode) && selectedNode.id === d.id) {
                deselectObject();
            } else {
                // Switching selection to new cluster
                if (d.isCluster) {
                    selectObject(d.url);
                } else {
                    selectObject(d.clusterUrl, d.index);
                }
            }
        }


        var width = 555,
            height = 600,
            radius = Math.min(width, height) / 2,
            centerRadius = radius / 3, // Only valid if the cluster depth stays at 2
            buffer = 10;

        var svg = d3.select(targetSelector).append("svg")
            .attr("width", "100%")
            .attr("height", "100%")
            .attr("viewBox", "0 0 " + width + " " + height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height * .52 + ")");

        var subclustersG = svg.append("g");

        var partition = d3.layout.partition()
            .sort(null)
            .size([2 * Math.PI, radius])
            .value(function(d) { return 1; })
            .children(function(d) { return d.members; });

        var arc = d3.svg.arc()
            .startAngle(function (d) { return d.x; })
            .endAngle(function (d) { return d.x + d.dx; })
            .innerRadius(function (d) { return d.y; })
            .outerRadius(function (d) { return d.y + d.dy; });

        var subclustersPaths = subclustersG.datum(root).selectAll("path")
            .data(partition.nodes)
            .enter().append("path")
            .attr("display", function (d) { return d.depth ? null : "none"; }) // hide inner ring
            .attr("d", arc)
//                    .attr("title", function(d) { return d.tooltip; })
            .attr("id", function (d) { return 'path' + idFromPath(d.id); })
            .style("stroke", "#fff")
            .style("fill", function(d) { return baseColor(d); })
            .style("fill-rule", "evenodd")
            .style("cursor", "hand")
            .on("click", onClick)
            .on("mouseover", onMouseover)
            .on("mouseout", onMouseout)
            .each(stash);

        var ancestorsG = svg.append("g");

        var ancestorsArc = d3.svg.arc()
            .startAngle(0)
            .endAngle(2 * Math.PI)
            .innerRadius(0)
            .outerRadius(function (d) { return (centerRadius - buffer) * (1 - (d.height - 1) / ancestors.length); });

        var ancestorsPaths = ancestorsG.selectAll("path")
            .data(ancestors)
            .enter().append("path")
            .attr("d", ancestorsArc)
            .attr("id", function (d) { return 'path' + idFromPath(d.id); })
            .style("stroke", "#fff")
            .style("fill", function(d) { return baseColor(d); })
            .style("fill-rule", "evenodd")
            .style("cursor", "hand")
            .on("click", onClick)
            .on("mouseover", onMouseover)
            .on("mouseout", onMouseout)
            .each(stash);

        d3.select(self.frameElement).style("height", height + "px");

        // Store mapping of all nodes so that they can be easily retrieved
        var nodeMap = {}, i, d;
        for (i = 0; i < subclustersPaths.data().length; i++)
        {
            d = subclustersPaths.data()[i];
            if (!isUndefined(d.url))
            {
                nodeMap[d.url] = d;
            } else {
                nodeMap[d.clusterUrl + d.index] = d;
            }
        }
        for (i = 0; i < ancestors.length; i++)
        {
            d = ancestors[i];
            nodeMap[d.url] = d;
        }

        // Private methods and data for accessors below

        function getNode(clusterUrl, trackIndex) {
            return nodeMap[clusterUrl + (isUndefined(trackIndex) ? '' : trackIndex)];
        }

        function getElem(d) {
            return d3.select('#path' + idFromPath(d.id));
        }

        var state = {
            flashState: {}
        };

        // Return an object with methods that operate on the sunburst data
        return {
            showSelection: function () {
                var d = getNode(selectedNode.clusterUrl, selectedNode.trackIndex);
                var elem = getElem(d);

                var flash = function (node) {
                    if (!isNull(selectedNode) && selectedNode.id === node.id) {
                        if (state.flashState[node.id]) {
                            console.debug('Flash up ' + node.id);
                            elem.transition()
                                .duration(500)
                                .style("fill", brightenedColor(d));
                            state.flashState[node.id] = false;
                        } else {
                            console.debug('Flash down ' + node.id);
                            elem.transition()
                                .duration(500)
                                .style("fill", darkenedColor(d));
                            state.flashState[node.id] = true;
                        }
                        setTimeout(function () { flash(node); }, 600);
                    } else {
                        console.debug('Stop flash ' + node.id);
                        delete state.flashState[node.id];
                        elem.transition()
                            .duration(200)
                            .style("fill", baseColor(d));
                    }
                };
                console.debug('Start flash ' + selectedNode.id);
                state.flashState[selectedNode.id] = true;
                flash(selectedNode);
            }
        };
    }

    /*
     *  Data model functions
     */

    // Ensures that allData contains all cluster info from server to a
    // given depth, retrieving records where necessary
    function getRequiredObjects(url, depth, onFirstDataLoaded, onAllDataLoaded, progressMap, details) {
        if (isUndefined(progressMap)) {
            progressMap = [];
        }

        if (isUndefined(details)) {
            details = {totalRecords: 0, fromServer: 0};
        }

        // We have to retrieve `url`
        progressMap[url] = true;
        details.totalRecords++;

        function checkSubnodes() {
            // If depth is zero, don't check any more subnodes
            if (depth > 0) {
                // Otherwise, repeat the process for all subnodes
                var node = allData[url];
                if (!node.isLeaf) {

                        for (var i = 0; i < node.numSubclusters; i++) {
                            // Only pass the token if we have it and it's the last child
                            getRequiredObjects(url + 'Cluster' + i + '/',
                                depth - 1,
                                undefined,
                                onAllDataLoaded,
                                progressMap,
                                details);
                        }

                }
                // Track data is bundled in to leaf nodes, so nothing to do
            }

            // `url` has been retrieved
            delete progressMap[url];

            if (!isUndefined(onFirstDataLoaded)) {
                onFirstDataLoaded();
            }

            if (Object.keys(progressMap) == 0) {
                onAllDataLoaded(details);
            }
        }

        if (isUndefined(allData[url])) {
            // Need to retrieve base node
            $.getJSON(url + 'cluster.json', function(data) {
                // IMPORTANT: store this node to the global map
                allData[url] = data;
                details.fromServer++;
                checkSubnodes();
            });
        } else {
            // Base node already retrieved
            checkSubnodes();
        }
    }

    function clusterDataObject(url) {
        var data = allData[url];
        return {
            isCluster: true,
            id: url,
            url: url,
            topWord: data.hist.length > 0 ? data.hist[0].w : '' // In some degenerate cases, a cluster may have no words
        };
    }

    function subclusterVisualizationData(url, depth, index) {
        if (isUndefined(index)) {
            index = 0;
        }

        console.debug('Building visualization data for cluster ' + url);

        var root = clusterDataObject(url);
        var clusterData = allData[url];
        if (depth) {
            root.members = [];
            if (clusterData.isLeaf) {
                for (var i = 0; i < clusterData.tracks.length; i++) {
                    root.members.push({
                        isCluster: false,
                        id: url + 'track' + i,
                        index: i,
                        clusterUrl: url,
                        audioRoot: clusterData.audioRoot,
                        trackData: clusterData.tracks[i]
                     });
                }
            } else {
                for (var i = 0; i < clusterData.numSubclusters; i++) {
                    root.members.push(subclusterVisualizationData(url + 'Cluster' + i + '/', depth - 1, i));
                }
            }
        }
        return root;
    }

    function ancestorsVisualizationData(url) {
        var data = allData[url];

        var ancestorData = [];
        for (var i = 0; i < data.ancestors.length; i++) {
            ancestorData.push(clusterDataObject(data.ancestors[i].url));
            ancestorData[i].height = data.ancestors.length - i;
        }
        ancestorData.reverse();
        return ancestorData;
    }

    function getRandomTrack(url, numTracks) {
        var nextUrl = '',
            trackFrac = Math.random();

        for (var cluster = allData[url + nextUrl], trackIndex = Math.floor(trackFrac * cluster.size); !cluster.isLeaf; url += nextUrl, cluster = allData[url]) {
            var clusterIndex = -1,
                tracksSeen = 0;

            while (trackIndex >= tracksSeen) {
                clusterIndex++;
                nextUrl = 'Cluster' + clusterIndex + '/';
                tracksSeen += allData[url + nextUrl].size;
            }

            trackIndex = trackIndex - (tracksSeen - allData[url + nextUrl].size);
        }

        return {url: url, trackIndex: trackIndex};
    }

    /*
     *  Event handlers
     */

    // Bind the handler for browser history functions
    History.Adapter.bind(window,'statechange',function() {
        var state = History.getState();
        loadPageForData(state.data.url);
    });

    // Intercept in-app link clicks and trigger history state changes
    // instead of full reloads
    var $body = $('body');
    $body.on('click', 'a.ich-link', function (event) {
        var url = $(this).attr('href');
        navigateToCluster(url);
        return false;
    });

    $body.on('click', '.enqueue-track', function (event) {
        console.debug('Enqueinng track #' + selectedNode.trackIndex + ' from cluster ' + selectedNode.clusterUrl);
        enqueueTrack(selectedNode.clusterUrl, selectedNode.trackIndex);
    });

    $body.on('click', '#enqueueRandomTracks', function (event) {
        var numTracks = +$('#numRandomTracks').val();

        for (var i = 0; i < numTracks; i++) {
            var track = getRandomTrack(selectedNode.clusterUrl);
            enqueueTrack(track.url, track.trackIndex);
        }
    });

    /*
     *  On-load code
     */

    $.ajax('/allData.json', {
        dataType: 'json',
        success: function (data) {
            console.debug('allData.json loaded')
            allData = data;
            incrementalLoad = false;
        },
        complete: function () {
            $('#loading-modal').modal('hide');

            // Perform the inital page load
            loadPageForData(window.location.pathname);
        }
    });

    $('#loading-modal').modal('show');

});
