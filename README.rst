Rake'Em Music Collection Explorer
=================================

Rake'Em is a web-based program for exploring very large music collections
intuitively. It is named after the recursive k-means (RKM) algorithm used to
produce the hierarchical data strucuture that underlies the exploration
interface. Included are the following:

-   ``kmeanshier.py`` - contains function used to generate the RKM data
    structure form audio features and for outputting the JSON file for powering
    the web interface.
-   ``webroot/`` - contains all the files for the Rake'Em web application.
-   ``webroot/allData.json`` - pregenerated collection data.
-   ``dotcloud.yml`` - build file for deploying to  `Dotcloud`_.

.. _Dotcloud: http://www.dotcloud.com/