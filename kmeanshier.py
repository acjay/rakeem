"""
Provides funcitons for computing the recursive k-means (RKM) and dumping the
data in the format used by the Rake'Em Collection Explorer interface.
"""
import codecs
from collections import Counter
from itertools import izip_longest
import json
import numpy as np
import os
import scipy.cluster.vq as vq
import scipy.spatial.distance as d
import traceback


class Cluster(object):
    """
    Datatype for containing cluster data
    """
    def __init__(
            self, members, depth, totalPoints, isLeaf, centroid,
            distortion=None):
        self.members = members
        self.depth = depth
        self.total_points = totalPoints
        self.is_leaf = isLeaf
        self.words = Counter()
        self.centroid = centroid
        self.distortion = distortion
        self.pdist = None
        self.pdistMin = None
        self.pdistMax = None
        self.representatives = None
        self.descendent_counts = None

    def __repr__(self):
        leafStr = 'Leaf' if self.is_leaf else 'Branch'
        return '{0}, Depth: {1}, # points: {2}'.format(
            leafStr, self.depth, self.total_points)


def k_means_hier(
        obs, nominalK, minSize, ids=None, obsW=None, depth=0, centroid=None):
    """
    Computes the recursive k-means (RKM) structure for the features of a track
    collection.

    :param obs: feature matrix
    :param nominalK: nominal number of subnodes per node
    :param minSize: minimal cluster size before nodes are bumped up to parent
    :param ids: indexes of tracks under node (used during recursion, do not set)
    :param obsW: whitened observation matrix (used during recursion, do not set)
    :param depth: current depth level (used during recursion, do not set)
    :param centroid: centroid of parent node (used during recursion, do not set)
    :return: RKM structure root node
    """
    print 'Depth: {0}, # points: {1}'.format(depth, obs.shape)

    if len(obs) < minSize * nominalK:
        k = int(len(obs) / minSize)
        if k <= 1:
            return Cluster(ids, depth, len(obs), True, centroid)
        doSubcluster = False
    else:
        k = nominalK
        doSubcluster = True

    if obsW is None:
        obsW = vq.whiten(obs)

    if ids is None:
        ids = np.arange(len(obs))

    if centroid is None:
        centroid = np.mean(obsW, 0)

    print '   {0}-means'.format(k)
    codebook, distortion = vq.kmeans(obsW, k, 6)
    code, dist = vq.vq(obsW, codebook)

    # Find the order of the clusters by size
    #counts = [sum(1 for x in someVQ[0] if x == i) for i in range(k)]
    #clusterOrder = range(k).sort(key=lambda i: counts[i])

    clusters = [[] for _ in range(k)]
    for i in range(len(obs)):
        clusters[code[i]].append(i)

    clusters = [np.array(cluster) for cluster in clusters]

    print '   Lengths: {0}'.format([len(cluster) for cluster in clusters])

    if doSubcluster:
        try:
            return Cluster(
                [
                    k_means_hier(
                        obs[cluster],
                        nominalK,
                        minSize,
                        ids[cluster],
                        obsW[cluster],
                        depth + 1,
                        codebook[i]
                    )
                    for i, cluster in enumerate(clusters) if len(cluster)
                ],
                depth,
                len(obs),
                False,
                centroid,
                distortion
            )
        except:
            print 'obs shape: {0}'.format(obs.shape)
            print obs
            print ' '
            print 'clusters: {0}'.format(len(clusters))
            print ids
            traceback.print_exc()
            return None
    else:
        return Cluster(
            [Cluster(
                ids[cluster],
                depth + 1,
                len(ids[cluster]),
                True,
                codebook[i]
            ) for i, cluster in enumerate(clusters) if len(cluster)],
            depth,
            len(obs),
            False,
            centroid,
            distortion
        )


def write_cluster_files(
        root, allMetadata, metadataHeaders, outputDir, audioRoot,
        staticRoot=None, individualFiles=True):
    """
    Create the files consumed by the Rake'Em web app

    :param root: RKM root node
    :param allMetadata: list of metadata dicts corresponding to each track
    :param metadataHeaders: header list for metadata
    :param outputDir: output directory for data
    :param audioRoot: where to find the audio files
    :param staticRoot: where to find the static files (used for local
        deployment, leave as None in most cases)
    :param individualFiles: set as True to dump individual files per cluster
    """

    # Normalize folder paths to have no slashes
    if audioRoot[-1] == '/':
        audioRoot = audioRoot[:-1]

    headerMap = dict(
        (key, position) for position, key in enumerate(metadataHeaders))

    def get_metadata(point, header):
        """
        Convenience function to retrieve metadata

        :param point: track index number
        :param header: header name
        :return: header text
        """
        metadata = allMetadata[point][headerMap[header]].decode('utf-8')
        return metadata if metadata else u'(unavailable)'

    allData = {}

    def descend(cluster, outputSubdir, clusterPath=None):
        """
        Recursively escend the RKM structure to build the output data

        :param cluster: RKM root node
        :param outputSubdir: directory to dump individual cluster output files
        :param clusterPath: list of parents to the current cluster (used in
            recursion, do not set)
        """
        check_create_dir(outputSubdir)

        if not clusterPath:
            clusterPath = [u'Root']

        print u'Processing {0}.  Output to {1}'.format(
            u'->'.join(clusterPath), outputSubdir)

        if not cluster.is_leaf:
            # Recurse over subclusters
            for i, subcluster in enumerate(cluster.members):
                descend(
                    subcluster,
                    os.path.join(outputSubdir, u'Cluster{0}'.format(i)),
                    clusterPath + [u'{0}'.format(i)]
                )
        else:
            # Prepare template context
            trackList = [
                {
                    u'index': idx,
                    u'url': get_metadata(point, u'remotePath')[25:],
                    u'title': get_metadata(point, u'title'),
                    u'artist': get_metadata(point, u'artist'),
                    u'album': get_metadata(point, u'album'),
                    u'trackNum': get_metadata(point, u'tracknum'),
                    u'genre': get_metadata(point, u'genre')
                }
                for idx, point in enumerate(cluster.members)
            ]

        get_path = lambda cp: '/' + ''.join(
            'Cluster{0}/'.format(a) for a in cp[1:])

        clusterUrls = [
            get_path(clusterPath[0:i + 1]) for i in range(cluster.depth)]

        context = {
            u'audioRoot': audioRoot + '/',
            u'ancestors': [
                {'url': url, 'name': name}
                for url, name in zip(clusterUrls, clusterPath[:-1])
            ],
            u'size': cluster.total_points,
            u'descendent_counts': cluster.descendent_counts,
            u'isLeaf': cluster.is_leaf,
            u'hist': [
                {'w': word, 'c': count}
                for word, count in cluster.words.most_common(30)
            ],
            u'representatives': [
                {
                    u't': get_metadata(point, u'title'),
                    u'a': get_metadata(point, u'artist'),
                }
                for point in cluster.representatives
            ]
        }

        if cluster.is_leaf:
            context[u'tracks'] = trackList
        else:
            context[u'numSubclusters'] = len(cluster.members)

        if individualFiles:
            outputFile = os.path.join(outputSubdir, u'cluster.json')
            with codecs.open(outputFile, 'w', encoding='utf-8') as f:
                json.dump(context, f, separators=(',', ':'))

        allData[get_path(clusterPath)] = context

        if outputDir != outputSubdir and staticRoot:
            # Don't make link for root directory
            htmlFile = os.path.join(outputDir, 'index.html')
            os.symlink(htmlFile, os.path.join(outputSubdir, 'index.html'))

    descend(root, outputDir)

    check_create_dir(outputDir)

    print 'Wrting all data file'
    clusterFile = os.path.join(outputDir, u'allData.json')
    with codecs.open(clusterFile, 'w', encoding='utf-8') as f:
        json.dump(allData, f, separators=(',', ':'))

    if staticRoot:
        if staticRoot[-1] == '/':
            staticRoot = staticRoot[:-1]

        print 'Linking files in static dir {0} to {1}'.format(
            staticRoot, outputDir)
        for item in os.listdir(staticRoot):
            print '    {0} -> {1}'.format(
                os.path.join(staticRoot, item), os.path.join(outputDir, item))
            os.symlink(
                os.path.join(staticRoot, item), os.path.join(outputDir, item))


def postorder_depthfirst(root):
    """
    Runs post-order depth-first traversal on the RKM structure.

    Useful for doing bottom-up computation.

    :param root: RKM root node
    :return: node visit tuple list in the form (node, depth, parent visit info)
    """
    toVisit = [(root, 0, None)]
    currentIdx = 0

    # Queue up nodes to visit
    while currentIdx < len(toVisit):
        currentNode, depth, path = toVisit[currentIdx]
        if not currentNode.is_leaf:
            toVisit.extend([
                (node, depth + 1, (currentNode, depth, path))
                for node in currentNode.members])
        currentIdx += 1

    # Yield children
    return [x for x in reversed(toVisit)]


def post_process_clusters(root, features, allMetadata, headers):
    """
    Runs postprocessing on RKM hierarchy

    :param root: RKM root node
    :param features: feature matrix
    :param allMetadata: list of metadata dicts corresponding to each track
    :param headers: header list for metadata
    """
    histogram_words(root, allMetadata, headers)
    assign_pdist(root, features)
    descendent_counts(root)
    find_representatives(root, features)


def histogram_words(
        root, allMetadata, headers,
        targetHeaders=('title', 'artist', 'album', 'genre')):
    """
    Creates a word histogram for all clusters in the hierachy.

    Counts the occurrences of all words in metadata fields for all tracks in
    each cluster, from the bottom up, allowing clusters to be differentiated by
    most common metadata words.

    Stores the counter mapping words to word counts as `counter` on each
    cluster.

    :param root: RKM root node
    :param allMetadata: list of metadata dicts corresponding to each track
    :param headers: header list for metadata
    :param targetHeaders: headers to mine
    """
    excludedWords = {
        ['the', 'of', 'in', 'a', 'and', 'to', 'for', 'on',
         'at', 'is', 'from', 'with', 'an', 'it', 'me', 'my']}

    def sanitize(string, header):
        """
        Clean up metadata text for histogramming

        :param string: header string
        :param header: header name
        :return: sanitized header text
        """

        import re

        # Lower case
        string = string.lower()
        # Remove apostrophes
        string = re.sub(r'([a-z])\'([a-z])', r'\1\2', string)
        # For genres, contract names
        # NOTE: instead of contracting names, could just store as tuples instead
        if header == 'genre':
            string = re.sub(r'[ -]', '', string)
        # Turn non alphabetic characters into separators
        string = re.sub('[^a-z]+', ' ', string)
        return string

    headerMap = dict((key, position) for position, key in enumerate(headers))
    for cluster, depth, parent in postorder_depthfirst(root):
        if cluster.is_leaf:
            allHeaderData = ' '.join(
                [
                    ' '.join(
                        [
                            sanitize(
                                allMetadata[pointIdx][headerMap[header]],
                                header)
                            for header in targetHeaders
                        ]
                    ) for pointIdx in cluster.members
                ]
            )
            cluster.words = Counter()
            cluster.words.update(
                word for word in allHeaderData.split()
                if word not in excludedWords and len(word) > 1)
        else:
            cluster.words = Counter()
            for subcluster in cluster.members:
                cluster.words.update(subcluster.words)


def assign_pdist(root, features):
    """
    Calculate the pairwise distance matrix of all subnodes for each cluster.

    Gives a way of measuring cohesiveness of each cluster and finding outlier
    nodes. Cluster cenroid is treated as the "0th" subnode, and the actual
    cluster subnodes count from index 1.

    Stores the pairwise distance matrix as `pdist` on each cluster, and store
    minimum and maximum distances as `pdistMin` and `pdistMax`.

    :param root: RKM root node
    :param features: feature matrix
    """
    featuresWhitened = vq.whiten(features)

    for cluster, depth, parent in postorder_depthfirst(root):
        if cluster.is_leaf:
            constituentPoints = [
                featuresWhitened[idx] for idx in cluster.members]
        else:
            constituentPoints = [
                subcluster.centroid for subcluster in cluster.members]

        constituentPoints = np.array([cluster.centroid] + constituentPoints)
        print u'{0}{1}'.format(
            ''.join(['\t' for _ in range(depth)]), constituentPoints.shape)
        pdist = d.pdist(constituentPoints)

        cluster.pdist = d.squareform(pdist).tolist()
        cluster.pdistMin = np.min(pdist)
        cluster.pdistMax = np.max(pdist)


def descendent_counts(root):
    """
    Counts the number of descendents at each depth.

    :param root: RKM root node
    """
    for cluster, depth, parent in postorder_depthfirst(root):
        if cluster.is_leaf:
            cluster.descendent_counts = [1]
        else:
            cluster.descendent_counts = [
                sum(levelCounts)
                for levelCounts in izip_longest(*[
                    subcluster.descendent_counts
                    for subcluster in cluster.members
                ], fillvalue=0)
            ]
            cluster.descendent_counts = [1] + cluster.descendent_counts


def find_representatives(root, features, numReps=3):
    """
    Finds represenative tracks for every cluster.

    Representative tracks are going to be the X closest to the centroid, chosen
    from all of the cluster's tracks.

    Stores the list of representatives as the `representatives` object property
    on each corresponding cluster.

    :param root: RKM root node
    :param features: feature matrix
    :param numReps: number of representative tracks to find
    """
    featuresWhitened = vq.whiten(features)

    for cluster, depth, parent in postorder_depthfirst(root):
        if cluster.is_leaf:
            candidates = cluster.members
        else:
            candidates = np.concatenate([
                subcluster.representatives for subcluster in cluster.members
            ])
        dist = d.cdist(
            cluster.centroid[np.newaxis, :], featuresWhitened[candidates])
        order = np.argsort(dist).flatten()
        chosenReps = order[np.arange(min(numReps, len(order)))]
        cluster.representatives = candidates[chosenReps].tolist()


def check_create_dir(path):
    """
    Basically mkdir -p.  Creates a directory if it doesn't already exist

    :param path: directory path
    """
    if not os.path.exists(path):
        os.makedirs(path)